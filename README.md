# vue-electron

## install .deb file
```
sudo dpkg -i package_file.deb
sudo apt-get remove package_name
```

## Run build for electron 
```
yarn e:build --dir --armv7l --linux deb
```

## Fix error by fpm with no arm7l arch
```
sudo apt install ruby ruby-dev
sudo gem install fpm
export USE_SYSTEM_FPM=true
```

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
